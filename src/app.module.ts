import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ProductModule } from './product/product.module';
import { MongooseModule } from '@nestjs/mongoose';
import { TmbModule } from './tmb/tmb.module';
@Module({
  imports: [ProductModule, MongooseModule.forRoot('mongodb+srv://zigo:OoFwSROhoq6YaMrQ@cluster0-p32ho.mongodb.net/nestjs-sheep?retryWrites=true&w=majority'), TmbModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule { }
