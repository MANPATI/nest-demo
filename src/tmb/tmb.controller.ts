import { Controller, Get, Param, Post, Body } from '@nestjs/common';
import { TmbService } from './tmb.service';

@Controller('mule')
export class TmbController {
  constructor(private readonly tmbService: TmbService) {}
  @Get('getAPIPublicList')
  getAPIPublicList() {
    return this.tmbService.getAPIPublicList();
  }

  @Get('getAPIDetail/:assetId/:groupId/:organizationId/:version')
  async getAPIDetail(@Param() req: any) {
    console.log(req);
    const data = await this.tmbService.getAPIDetail(req.assetId);
    console.log('data', data);
    return data;
  }

  @Post('/catalog/find')
  async catalog(@Body() req: any) {
    const data = await this.tmbService.getAssect(req.assetId);
    return data;
  }
}
