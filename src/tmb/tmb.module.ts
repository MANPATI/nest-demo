import { Module } from '@nestjs/common';
import { TmbService } from './tmb.service';
import { TmbController } from './tmb.controller';

@Module({
  providers: [TmbService],
  controllers: [TmbController]
})
export class TmbModule {}
