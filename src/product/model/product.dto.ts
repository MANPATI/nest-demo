import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";
import { IsString, IsNumber } from "class-validator";

export class ProductDto {
  @ApiProperty()
  @IsString()
  title: string;
  @ApiPropertyOptional()
  @IsString()
  description: string;
  @ApiProperty()
  @IsNumber()
  price: number;
}