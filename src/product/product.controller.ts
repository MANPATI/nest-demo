import {
  Controller,
  Post,
  Body,
  Get,
  Patch,
  Param,
  Delete,
  UsePipes,
  HttpException,
  UseInterceptors,
  UseFilters,
} from '@nestjs/common';
import { ProductService } from './product.service';
import { Product } from './model/product.model';
import { ProductDto } from './model/product.dto';
import { ValidatePipe } from 'src/shared/validate/validate.pipe';
import { ApiForbiddenResponse, ApiResponse, ApiBody } from '@nestjs/swagger';
import { PostInterceptor } from 'src/shared/interceptor/post.interceptor';
import { HttpExceptionFilter } from 'src/shared/interceptor/http-exception.filter';

@Controller('product')
@UsePipes(new ValidatePipe())
export class ProductController {
  constructor(private readonly productsService: ProductService) {}

  @Post()
  @ApiResponse({
    status: 201,
    description: 'The record has been successfully created.',
    schema: {
      type: 'object',
      properties: {
        statusCode: { type: 'number' },
        message: { type: 'string' },
        data: {
          type: 'object',
          properties: {
            id: { type: 'string' },
          },
        },
      },
    },
  })
  /* @ApiBody({
    description: 'List of cats',
    schema: {
      type: 'object',
      properties: {
        prodTitle: { type: 'string' },
        prodDesc: { type: 'string' },
        prodPrice: { type: 'number' },
      }
    }
  }) */
  async addProduct(@Body() prodductDto: ProductDto) {
    const { title, description, price } = prodductDto;
    return await this.productsService.insertProduct(title, description, price);
  }

  @Get()
  async getAllProducts() {
    const products = await this.productsService.getProducts();
    return products;
  }

  @Get(':id')
  getProduct(@Param('id') prodId: string) {
    return this.productsService.getSingleProduct(prodId);
  }

  @Patch(':id')
  async updateProduct(
    @Param('id') prodId: string,
    @Body('title') prodTitle: string,
    @Body('description') prodDesc: string,
    @Body('price') prodPrice: number,
  ) {
    await this.productsService.updateProduct(
      prodId,
      prodTitle,
      prodDesc,
      prodPrice,
    );
    return null;
  }

  @Delete(':id')
  async removeProduct(@Param('id') prodId: string) {
    await this.productsService.deleteProduct(prodId);
    return null;
  }
}
